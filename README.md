# Final project for Innopolis University DevOps course.
## Develop simple Flask app and CI/CD pipeline to build and deploy to Yandex cloud.

* Monitoring: Prometheus, Grafana
* Logging: ELK
* IaC: Ansible
* Web server: Nginx
* Build: Docker.
* Environment: VM in Yandex cloud.
* Autor: Marat Biryushev.
